# Industrial-Control

## 概述
Industrial-Control SIG组主要致力于将openEuler打造成适用于工业控制领域或嵌入式领域的实时操作系统。


## 工作职责
 - 制定Xenomai实时方案相关软件包的版本生命周期
 - 对常见开源工业控制现场总线，如Modbus、CANopen、EtherCAT等进行迁移、适配和优化
 - 移植实时相关或适用于工业控制领域的虚拟化方案
 - 移植RTOS系统，如Zephyr
 - 复用embedded SIG组成果，为嵌入式系统提供实时方案
 - 回馈上游社区
 - 及时响应用户反馈，解决相关问题
 - 引入其他工业控制相关特性及新技术

## 目标
 本SIG组将会把强实时Xenomai方案引入openEuler社区中，并对常见的工业控制现场总线进行迁移、适配和优化，将openEuler打造成可以应用于工业控制领域的操作系统。
 
 另一方面，随着硬件技术的发展，实时特性的硬件虚拟化也将会成为未来发展方向，本SIG组将致力于实时虚拟化方向的研究，让常见的RTOS，如Zephyr、RTEMS、FreeRTOS等系统可同时与openEuler运行在同一平台上，满足某几个核心运行实时任务，其他核心运行通用任务的新型工业控制相关领域的需求。

# 组织会议
- 公开的会议时间：北京时间，每双周三下午，15点~17点

# 成员

### Maintainer列表
- 黎亮[@liliang_euler](https://gitee.com/liliang_euler), liliang889@huawei.com
- 张攀[@SuperHugePan](https://gitee.com/SuperHugePan), zhangpan26@huawei.com
- 郭皓[@guohaoc2c2](https://gitee.com/guohaocs2c) , guohao@kylinos.cn
- 吴春光[@wuchunguang](https://gitee.com/wuchunguang) , wuchunguang@kylinos.cn
- 马玉昆[@kylin-mayukun](https://gitee.com/kylin-mayukun) , mayukun@kylinos.cn
- 张茜[@zxiiiii](https://gitee.com/zxiiiii) , zhangxi@kylinos.cn
- 丁丽丽[@blueskycs2c](https://gitee.com/blueskycs2c) , dinglili@kylinos.cn
- 张继文[@zhang-jiwen](https://gitee.com/zhang-jiwen), zhangjiwen@kylinos.cn

### Committer列表


# 联系方式
- [邮件列表](dev@openeuler.org)
- [IRC频道](#openeuler-dev)
- [IRC公开会议](#openeuler-meeting)
